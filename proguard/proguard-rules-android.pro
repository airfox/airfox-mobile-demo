-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keep public class * extends android.preference.Preference {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keep class * extends java.util.ListResourceBundle {
    protected java.lang.Object[][] getContents();
}

# Keep SafeParcelable value, needed for reflection. This is required to support backwards
# compatibility of some classes.
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

# Keep the names of classes/members we need for client functionality.
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepattributes EnclosingMethod

-keep class com.mgensuite.datarewards.DataRewards
-keepclassmembers class com.mgensuite.datarewards.DataRewards {*;}
-keep class com.mgensuite.datarewards.DataRewardsBuilder
-keepclassmembers class com.mgensuite.datarewards.DataRewardsBuilder {*;}
-keep class com.mgensuite.datarewards.DataRewardsModule
-keepclassmembers class com.mgensuite.datarewards.DataRewardsModule {*;}
-keep interface com.mgensuite.datarewards.DataRewardsModule$Callback {*;}
-keepclassmembers class com.mgensuite.datarewards.Data {*;}
-keep interface com.mgensuite.datarewards.Data {*;}
-keepclassmembers class com.mgensuite.datarewards.DataBuilder {*;}
-keep class com.mgensuite.datarewards.DataBuilder {*;}
-keepclassmembers class com.mgensuite.datarewards.DataInitializer {*;}
-keep class com.mgensuite.datarewards.DataInitializer {*;}

-keepclasseswithmembers public class com.mgensuite.datarewards.activity.HomeActivity {*;}

-keep class com.mgensuite.datarewards.util.phonefield.Countries
-keepclassmembers class com.mgensuite.datarewards.util.phonefield.Countries {*;}
-keep class com.mgensuite.datarewards.util.phonefield.Country
-keepclassmembers class com.mgensuite.datarewards.util.phonefield.Country {*;}

-keepclasseswithmembers class com.mgensuite.datarewards.model.** {*;}

# this is needed for the upgrade mechanism to work
-keep class com.mgensuite.sdk.core.AppInstaller
-keepclassmembers class com.mgensuite.sdk.core.AppInstaller {*;}
-keep class android.content.pm.IPackageInstallObserver
-keepclassmembers class android.content.pm.IPackageInstallObserver {*;}
-keep class android.content.pm.IPackageDeleteObserver
-keepclassmembers class android.content.pm.IPackageDeleteObserver {*;}

-keep class com.mgensuite.sdk.core.IAirFoxMobileCore
-keep class com.mgensuite.sdk.core.IConfigurationCallback
-keep class com.mgensuite.sdk.core.IRestCallback
-keep class com.mgensuite.sdk.core.ISubscriptionCallback
-keep class com.mgensuite.sdk.core.IUpdateCallback

-keep class com.mgensuite.lockscreen.LockScreen
-keepclassmembers class com.mgensuite.lockscreen.LockScreen {*;}
-keep class com.mgensuite.lockscreen.LockScreenBuilder
-keepclassmembers class com.mgensuite.lockscreen.LockScreenBuilder {*;}
-keep class com.mgensuite.lockscreen.LockScreenModule
-keepclassmembers class com.mgensuite.lockscreen.LockScreenModule {*;}
-keep interface com.mgensuite.lockscreen.LockScreenModule$Callback {*;}
-keep class com.mgensuite.lockscreen.contentfeed.handler.notification.NotificationBuilder
-keepclassmembers class com.mgensuite.lockscreen.contentfeed.handler.notification.NotificationBuilder {*;}
-keep class com.mgensuite.lockscreen.contentfeed.handler.notification.NotificationFormat
-keepclassmembers class com.mgensuite.lockscreen.contentfeed.handler.notification.NotificationFormat {*;}
-keep class com.mgensuite.lockscreen.contentfeed.handler.notification.Notification
-keepclassmembers class com.mgensuite.lockscreen.contentfeed.handler.notification.Notification {*;}

-keepclasseswithmembers class com.mgensuite.lockscreen.model.** {*;}

-keepclassmembers class * extends java.lang.Enum {
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

# EventBus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

-keep class com.mgensuite.lockscreen.ui.UnlockedEvent
-keepclassmembers class com.mgensuite.lockscreen.ui.UnlockedEvent {*;}

-keep class com.fyber.mediation.annotations.** { *; }
-keep class com.fyber.annotations.** { *; }
-dontwarn com.fyber.mediation.annotations.**
-dontwarn com.fyber.annotations.**

# Google Advertising Id
-keep class com.google.android.gms.ads.identifier.** { *; }

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

-keep class me.kiip.** { *; }
-keepclassmembers class me.kiip.** {*;}

-dontwarn com.squareup.okhttp.**

-keepclassmembers class com.supersonicads.sdk.controller.SupersonicWebView$JSInterface {
    public *;
}
-keepclassmembers class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
}
-keep public class com.google.android.gms.ads.** {
   public *;
}
-keep class com.supersonic.adapters.** { *;
}

-keepattributes InnerClasses
# JS Interfaces and other reflection/dynamic invokations are not
# properly tracked by obfuscation engine
# Prevent issues with Google GAID
-keep public class com.google.** {
    *;
}
# Prevent renaming/obfuscation of JS Interfaces,
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# Keep public classes and methods.
-keep class com.avocarrot.** { *; }
-dontwarn com.avocarrot.**

-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

-keepattributes SourceFile,LineNumberTable,InnerClasses
-keep class com.inmobi.** { *; }
-dontwarn com.inmobi.**
-keep public class com.google.android.gms.**
-dontwarn com.google.android.gms.**
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient{
     public *;
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info{
     public *;
}
-dontwarn okio.**
# skip the Picasso library classes
-keep class com.squareup.picasso.** {*;}
-dontwarn com.squareup.picasso.**
-dontwarn com.squareup.okhttp.**
# skip Moat classes
-keep class com.moat.** {*;}
-dontwarn com.moat.**

##---------------Begin: proguard configuration for Inneractive SDK  ----------

-keep class com.inneractive.api.ads.** {*;}
-keepclassmembers class com.inneractive.api.ads.** {*;}
-keepclassmembers class com.inneractive.api.ads.sdk.nativead.** {*;}

##---------------End: proguard configuration for Inneractive SDK  ----------


##---------------Begin: proguard configuration for Google play services  ----------

-dontwarn com.google.android.gms.**

# for Google play services – general – based on Google's documentation
-keep class * extends java.util.ListResourceBundle
{ protected Object[][] getContents(); }
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable
{ public static final *** NULL; }
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class *
{ @com.google.android.gms.common.annotation.KeepName *; }
-keepnames class * implements android.os.Parcelable
{ public static final ** CREATOR; }

# for google play services, to support Inneractive’s reflection
-keep class com.google.android.gms.common.GooglePlayServicesUtil
{ int isGooglePlayServicesAvailable (android.content.Context); }

-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient
{ static com.google.android.gms.ads.identifier.AdvertisingIdClient$Info getAdvertisingIdInfo(android.content.Context);}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info
{ *; }
-keep class com.google.android.gms.common.GoogleApiAvailability
{ static com.google.android.gms.common.GoogleApiAvailability getInstance(); int isGooglePlayServicesAvailable(android.content.Context);}

##---------------End: proguard configuration for Google play services  ----------


##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

-keep class com.google.gson.Gson { *; }
-keep class com.google.gson.GsonBuilder { *; }
-keep class com.google.gson.FieldNamingStrategy { *; }

##---------------End: proguard configuration for Gson  ----------

# Millenial
-keepclassmembers class com.millennialmedia** {
public *;
}

-keep class com.millennialmedia**

-dontwarn com.moat.**

# MoPub Proguard Config
# NOTE: You should also include the Android Proguard config found with the build tools:
# $ANDROID_HOME/tools/proguard/proguard-android.txt

# Keep public classes and methods.
-keepclassmembers class com.mopub.** { public *; }
-keep public class com.mopub.**
-keep public class android.webkit.JavascriptInterface {}

# Explicitly keep any custom event classes in any package.
-keep class * extends com.mopub.mobileads.CustomEventBanner {}
-keep class * extends com.mopub.mobileads.CustomEventInterstitial {}
-keep class * extends com.mopub.nativeads.CustomEventNative {}
-keepclassmembers class com.mopub.mobileads.CustomEventBannerAdapter {
    !private !public !protected *;
}

# Support for Android Advertiser ID.
-keep class com.google.android.gms.common.GooglePlayServicesUtil {*;}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient {*;}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info {*;}

# Support for Google Play Services
# http://developer.android.com/google/play-services/setup.html
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

# Keep public classes and methods.
-keepattributes Signature
-keep class net.pubnative.** { *; }
