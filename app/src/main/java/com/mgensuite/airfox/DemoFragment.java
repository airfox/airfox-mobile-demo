package com.mgensuite.airfox;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;

import com.mgensuite.ads.handler.notification.NotificationBuilder;
import com.mgensuite.ads.handler.notification.NotificationFormat;
import com.mgensuite.airfox.demoapp.R;
import com.mgensuite.datarewards.activity.SplashActivity;
import com.mgensuite.lockscreen.LockScreen;
import com.mgensuite.lockscreen.LockScreenModule;

public class DemoFragment extends PreferenceFragment {

    private final String IS_NATIVE_DISPLAYED = "isNativeDisplay";

    public static DemoFragment getInstance() {
        return new DemoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Context context = getActivity();

        addPreferencesFromResource(R.xml.demo_preferences);

        final LockScreen ls = AirFoxDemoApp.getLockScreen();

        /*
         * Lock Screen - Ad Type
         */
        String key = getString(R.string.preference_list_selection_ad_key);
        final ListPreference adPreference = (ListPreference)getPreferenceScreen().findPreference(key);

        adPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                if (value instanceof String) {
                    updateAdPreference(context, adPreference, (String) value, ls);
                }
                return true;
            }
        });

        CharSequence value = adPreference.getValue();
        int index = value != null ? adPreference.findIndexOfValue(value.toString()) : 0;
        if (index == -1) index = 0;
        String entryValue = adPreference.getEntryValues()[index].toString();
        updateAdPreference(context, adPreference, entryValue, ls);

        /*
         * Lock Screen - Post-Unlock
         */
        key = getString(R.string.preference_enable_post_unlock_key);
        final SwitchPreference togglePostUnlock = (SwitchPreference) findPreference(key);

        togglePostUnlock.setChecked(ls.isPostUnlockAdEnabled(context));

        togglePostUnlock.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                ls.enablePostUnlockAd(context, (Boolean) newValue);
                return true;
            }
        });

        /*
         * Lock Screen - Notification
         */
        Preference notificationPreference = findPreference("notification");
        if (notificationPreference != null) {
            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            notificationPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if (prefs.getBoolean(IS_NATIVE_DISPLAYED, false)) {
                        displayMrecNotification();
                        prefs.edit().putBoolean(IS_NATIVE_DISPLAYED, false).commit();
                    } else {
                        displayNativeNotification();
                        prefs.edit().putBoolean(IS_NATIVE_DISPLAYED, true).commit();
                    }

                    return true;
                }
            });
        }

        /*
         * Rewards - Compact Header
         */
        final SwitchPreference toggleCompactLayout = (SwitchPreference) findPreference(getString(R.string.preference_enable_compactlayout_key));
        Boolean useCompactLayout = AirFoxDemoApp.getDataRewards().isCompactLayoutEnabled();
        if (useCompactLayout == null) {
            int[] attrs = new int[] { com.mgensuite.datarewards.R.attr.datarewardsCompactLayout};
            TypedArray typedArray = context.obtainStyledAttributes(attrs);
            useCompactLayout = typedArray.getBoolean(0,false);
        }

        toggleCompactLayout.setChecked(useCompactLayout);

        toggleCompactLayout.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                return toggleCompactLayout((Boolean) newValue);
            }
        });

        /*
         * Rewards - Launch
         */
        Preference launchDataRewardsButton = findPreference(getString(R.string.preference_launch_datarewards_key));
        if (launchDataRewardsButton != null) {
            launchDataRewardsButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    launchDataRewards();
                    return true;
                }
            });
        }
    }

    private void updateAdPreference(Context context, ListPreference adPreference, String value,
                                    LockScreen ls) {
        String summary = "";
        switch (value) {
            case "1":
                ls.enableLockscreen(context, false);
                ls.enableAdWall(context, false);
                summary = getString(R.string.preference_ads_none);
                break;
            case "2":
                ls.enableLockscreen(context, true);
                ls.enableAdWall(context, false);
                summary = getString(R.string.preference_ads_lockscreen);
                break;
            case "3":
                ls.enableLockscreen(context, false);
                ls.enableAdWall(context, true);
                summary = getString(R.string.preference_ads_adwall);
                break;
        }

        String newSummary = getString(R.string.preference_ads_description, summary);
        adPreference.setSummary(newSummary);
    }

    private void displayNativeNotification() {

        new NotificationBuilder()
                .title("Reward")
                .description("Congrats! You earned $.30 already.")
                .callToAction("Show Balance")
                .callToActionIntent(new Intent(Intent.ACTION_VIEW, Uri.parse("http://airfox.io")))
// alternatively .callToActionUrl("http://www.airfox.io")
                .logoUrl("http://www.clker.com/cliparts/R/U/Y/u/I/M/thumbs-up-icon-blue-hi.png")
// alternatively .logoResId(R.drawable.myLogo)
                .imageUrl("http://www.childreninmedia.com/photos/57/images/cim000537.jpg")
// alternatively .imageResId(R.drawable.myImage)
                .background(0xff888888)
                .create(LockScreenModule.getAppUuid(), "lockscreen", NotificationFormat.NATIVE)
                .show();
    }

    private void displayMrecNotification() {
        new NotificationBuilder()
                .callToActionIntent(new Intent(Intent.ACTION_VIEW, Uri.parse("http://airfox.io")))
// alternatively .callToActionUrl("http://www.airfox.io")
                .imageUrl("http://www.childreninmedia.com/photos/57/images/cim000537.jpg")
// alternatively .imageResId(R.drawable.myImage)
                .background(0xff888888)
                .create(LockScreenModule.getAppUuid(), "lockscreen", NotificationFormat.MRECT)
                .show();
    }

    private boolean toggleCompactLayout(Boolean enabled) {
        AirFoxDemoApp.getDataRewards().enableCompactLayout(enabled);
        return true;
    }

    private void launchDataRewards() {
        Intent intent = new Intent(getActivity(), SplashActivity.class);

        startActivity(intent);
    }

}
