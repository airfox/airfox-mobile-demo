package com.mgensuite.airfox;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mgensuite.airfox.demoapp.R;

public class DemoPreferences {

    public static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean isLockScreenEnabled(Context context) {
        String key = context.getString(R.string.preference_enable_lockscreen_key);
        return getPreferences(context).getBoolean(key, false);
    }

    public static boolean isCompactLayoutEnabled(Context context) {
        String key = context.getString(R.string.preference_enable_compactlayout_key);
        return getPreferences(context).getBoolean(key, false);
    }

    public static boolean isTransparentLockScreenEnabled(Context context) {
        String key = context.getString(R.string.preference_enable_transparent_lockscreen_key);
        return getPreferences(context).getBoolean(key, false);
    }

}
