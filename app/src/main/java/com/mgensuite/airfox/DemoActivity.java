package com.mgensuite.airfox;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;

import com.mgensuite.ads.AdFormat;
import com.mgensuite.ads.AdMoment;
import com.mgensuite.airfox.demoapp.R;

public class DemoActivity extends AppCompatActivity {

    private static final AdMoment mBannerMoment = new AdMoment("demo_banner");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.app_name));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // show banner ads at the bottom of the screen
        ViewGroup adContainer = (ViewGroup)  findViewById(R.id.banner_demo_container);
        if (adContainer != null) {
            AirFoxDemoApp.getLockScreen().showAd(adContainer, mBannerMoment, AdFormat.BANNER, null);
        }
    }

}