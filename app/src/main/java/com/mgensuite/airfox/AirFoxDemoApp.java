package com.mgensuite.airfox;

import android.content.Intent;
import android.net.Uri;
import android.support.multidex.MultiDexApplication;

import com.mgensuite.ads.handler.notification.NotificationBuilder;
import com.mgensuite.ads.handler.notification.NotificationFormat;
import com.mgensuite.airfox.demoapp.BuildConfig;
import com.mgensuite.datarewards.DataRewards;
import com.mgensuite.datarewards.DataRewardsBuilder;
import com.mgensuite.datarewards.DataRewardsModule;
import com.mgensuite.lockscreen.LockScreen;
import com.mgensuite.lockscreen.LockScreenBuilder;
import com.mgensuite.lockscreen.LockScreenModule;
import com.mgensuite.sdk.core.util.Logger;

public class AirFoxDemoApp extends MultiDexApplication {

    private static LockScreen sLockScreen;
    private static DataRewards sRewards;

    public AirFoxDemoApp() {}

    public static LockScreen getLockScreen() {
        return sLockScreen;
    }

    public static DataRewards getDataRewards() {
        return sRewards;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // use this to enable debug logging for release builds (don't actually release this though):
        Logger.enableDebugLogging(BuildConfig.DEBUG);

        // this needs to be the first call to the lock screen module
        LockScreenModule.init(this, BuildConfig.DEBUG);

        LockScreenBuilder lsBuilder = new LockScreenBuilder(
                BuildConfig.DEBUG ? BuildConfig.app_key_lockscreen_staging : BuildConfig.app_key_lockscreen)
                .localWaterfall(BuildConfig.useLocalWaterfall);

        // this needs to be the first call to the rewards module
        DataRewardsModule.init(this, BuildConfig.DEBUG);

        DataRewardsBuilder drBuilder = new DataRewardsBuilder(
                BuildConfig.DEBUG ? BuildConfig.app_key_datarewards_staging : BuildConfig.app_key_datarewards)
                .allowRecharge(BuildConfig.allowRecharge);

        boolean hasUserName = ! BuildConfig.TEST_USER_NAME.isEmpty();
        boolean hasUserSecret = ! BuildConfig.TEST_USER_SECRET.isEmpty();
        if (hasUserName && hasUserSecret) {
            lsBuilder.userCredentials(BuildConfig.TEST_USER_NAME, BuildConfig.TEST_USER_SECRET);
            drBuilder.userCredentials(BuildConfig.TEST_USER_NAME, BuildConfig.TEST_USER_SECRET);
        } else {
            lsBuilder.mdn(BuildConfig.TEST_MDN).simcardId(BuildConfig.TEST_SIM_CARD);
            drBuilder.mdn(BuildConfig.TEST_MDN).simcardId(BuildConfig.TEST_SIM_CARD);
        }

        sLockScreen = lsBuilder.build();

        sRewards = drBuilder.build();

        sLockScreen.enableLockscreen(this, DemoPreferences.isLockScreenEnabled(this));
        //sLockScreen.enableAdWall(this, DemoPreferences.isTransparentLockScreenEnabled(this));

        // show/hide rewards notifications
        sRewards.notifications(BuildConfig.showNotifications);
        sRewards.enableCompactLayout(DemoPreferences.isCompactLayoutEnabled(this));

        // native notification
        new NotificationBuilder()
                .title("Rewards")
                .description("Congrats! You earned $.30 already.")
                .callToAction("Show Balance")
                .callToActionIntent(new Intent(Intent.ACTION_VIEW, Uri.parse("http://airfox.io")))
// alternatively         .callToActionUrl("http://www.airfox.io")
                .logoUrl("http://www.clker.com/cliparts/R/U/Y/u/I/M/thumbs-up-icon-blue-hi.png")
// alternatively               .logoResId(R.drawable.myLogo)
                .imageUrl("http://www.childreninmedia.com/photos/57/images/cim000537.jpg")
// alternatively               .imageResId(R.drawable.myImage)
                .background(0xff888888)
                .create(LockScreenModule.getAppUuid(), "lockscreen", NotificationFormat.NATIVE)
                .show();

        // mrect notification
        new NotificationBuilder()
                .callToActionIntent(new Intent(Intent.ACTION_VIEW, Uri.parse("http://airfox.io")))
// alternatively         .callToActionUrl("http://www.airfox.io")
                .imageUrl("http://www.childreninmedia.com/photos/57/images/cim000537.jpg")
// alternatively               .imageResId(R.drawable.myImage)
                .background(0xff888888)
                .create(LockScreenModule.getAppUuid(), "lockscreen", NotificationFormat.MRECT)
                .show();

        // lock the screen
//        sLockScreen.lock(this);

        // unlock the screen (only if no pin was set)
//        sLockScreen.unlock(this);

        // disable lock screen for this user (or simply don't initialize it above)
//        sLockScreen.enableLockscreen(this, false);

        // enable lock screen for this user (needs to be initialized to have an effect)
        // will turn off the ad-wall!
//        sLockScreen.enableLockscreen(this, true);

        // enable the pin, it needs to be 4 digits (if not the pin won't be used)
//        sLockScreen.enablePin(this, "1111");

        // disable the pin
//        sLockScreen.disablePin(this);

        // disable the ad-wall for this user (or simply don't initialize it above)
//        sLockScreen.enableAdWall(this, false);

        // enable the ad-wall for this user (needs to be initialized to have an effect)
        // will turn off the lockscreen!
//        sLockScreen.enableAdWall(this, true);

        // disable post-unlock ads for this user (or simply don't initialize it above)
//        sLockScreen.enablePostUnlockAd(this, false);

        // enable post-unlock ads for this user (needs to be initialized to have an effect)
//        sLockScreen.enablePostUnlockAd(this, true);
    }

}